const router = require('express').Router();
const auth = require('./routes/auth');
const users = require('./routes/users');
const messages = require('./routes/messages');
const rooms = require('./routes/rooms');

router.get("/",function(req,res){
  res.json({"error" : false, "message" : "Hello World"});
});

router.use(auth)
router.use(users)
router.use(messages)
router.use(rooms)

module.exports = router;