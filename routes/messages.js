const router = require('express').Router();
const Room = require('../models/room');
const Message = require('../models/message');

router.route("/messages")
.get((req, res) => {
  let dbFindParams = {}
  if (req.query.from || req.query.to) dbFindParams.created_at = {}
  if (req.query.from) dbFindParams.created_at.$gte = +req.query.from
  if (req.query.to) dbFindParams.created_at.$lte = +req.query.to

  Message.find(dbFindParams, { __v: 0 }, {sort: {created_at: 1}}, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    return res.status(200).send(data)
  })
})
.post((req, res) => {
  //params from body request TODO validation
  let newMessage = Message({
    msg: req.body.msg,
    user: {
      username: req.body.username,
      _id: req.body.user_id
    },
    roomId: req.body.roomId
  });

  let response = {};

  newMessage.save((err) => {
    if(err) {
      return res.status(400).send(err);
    }
    return res.status(200).json({message : "Message added successfully"})
  })
})

router.route("/messages/:id")
.get((req,res) => {
  Message.findById(req.params.id, { __v: 0 }, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    if(!data) {
      return res.status(200).json({message : "Found no message with ID: " + req.params.id})
    }
    return res.status(200).send(data);
  })
})
.put((req,res) => {
  Message.findById(req.params.id, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    if(!data) {
      return res.status(200).json({message : "Found no message with ID: " + req.params.id})
    } else {
      if(req.body.msg !== undefined) {
        data.msg = req.body.msg;
      }
      data.save((err) => {
        if(err) {
          return res.status(400).send(err);
        }
        return res.status(200).json({message : "Data is updated for " + req.params.id})
      })
    }
  })
})
.delete((req,res) => {
  Message.findById(req.params.id, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    if(!data) {
      return res.status(200).json({message : "Found no message with ID: " + req.params.id})
    } else {
      Message.remove({_id : req.params.id}, (err) => {
        if(err) {
          return res.status(400).send(err);
        }
        return res.status(200).json({message : "Data associated with " + req.params.id + " is deleted"})
      })
    }
  })
})

//get messages for room with provided room ID
router.route("/getmessages/:roomId")
.get((req,res) => {
  Message.find({ "roomId": req.params.roomId }, { __v: 0 }, {sort: {created_at: 1}}, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    return res.status(200).send(data);
  })
})

//get messages for all user rooms with provided user ID
router.route("/getroomsmessages/:userId")
.get((req,res) => {
  //get rooms containing user provided ID (data[room1, room2])
  Room.find({ "members": { $in: [req.params.userId] } }, { __v: 0 }, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    if(data.length === 0) {
      return res.status(200).send(data)
    }
    let roomsId = data.map(room => room._id)
    
    let dbFindParams = {"roomId": {$in: roomsId}}
    if (req.query.from || req.query.to) dbFindParams.created_at = {}
    if (req.query.from) dbFindParams.created_at.$gte = +req.query.from
    if (req.query.to) dbFindParams.created_at.$lte = +req.query.to
    
    Message.find(dbFindParams, { __v: 0 }, {sort: {created_at: 1}}, (err, data) => {
      if(err) {
        return res.status(400).send(err);
      }
      return res.status(200).send(data)
    })   
  })
})

module.exports = router;