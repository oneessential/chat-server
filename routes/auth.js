const router = require('express').Router();
const config = require('../config.json');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const isEmail = require( "sane-email-validation" );

router.route("/login")
.post((req, res) => {
  if(!req.body.email || !req.body.password) {
    return res.status(400).json({message: "Provide all login fields"})
  }
  if(!isEmail(req.body.email)) {
    return res.status(401).json({message: "Email is not valid"})
  }
  User.findOne({ 'email': req.body.email }, { __v: 0 }, (err, user) => {
    if(!user) {
      return res.status(404).json({message: "No user found with email: " + req.body.email})
    }
    if(err) {
      return res.status(400).send(err);
    }
    let reqPassword = require('crypto')
                    .createHash('sha1')
                    .update(req.body.password)
                    .digest('base64');

    let dbPassword = user.password

    if( dbPassword !== reqPassword ) {
      return res.status(401).json({message: "Wrong password"})
    }
    
    user.set('password', undefined, { strict: false})
  
    const token = jwt.sign(user, config.jwt_secret, { noTimestamp: true })
    return res.status(200).json({
            user,
            token,
            tokenType: 'Bearer'
          })
  })
})

router.route("/signup")
.post((req, res) => {
  let errorMsg = []
  if( !req.body.firstName ) {
    errorMsg.push("First name")
  }
  if( !req.body.lastName ) {
    errorMsg.push("Last name")
  }
  if( !req.body.email ) {
    errorMsg.push("Email")
  }
  if( !req.body.password ) {
    errorMsg.push("Password")
  }
  if( !req.body.confirmPassword ) {
    errorMsg.push("Confirm password")
  }
  if(errorMsg.length !== 0) {
    let message = errorMsg.join(', ')
    return res.status(400).json({message: "Provide " + message + " field"})
  }
  if(!isEmail(req.body.email)) {
    return res.status(401).json({message: "Email is not valid"})
  }
  if( req.body.password !== req.body.confirmPassword ) {
      return res.status(401).json({message: "Passwords didn't match"})
  }
  User.findOne({ "email": req.body.email }, (err, user) => {
    if(err) {
      return res.status(400).send(err);
    }
    if(user) {
      res.status(403).json({message: "User with this email already exist"})
    } else {
      let password = require('crypto')
                    .createHash('sha1')
                    .update(req.body.password)
                    .digest('base64');

      let newUser = User({
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        username : req.body.firstName + ' ' + req.body.lastName,
        email : req.body.email,
        password : password
      })

      newUser.save((err, user) => {
        if(err) {
          res.status(400).send(err);
        }

        user.set('password', undefined, { strict: false})
        const token = jwt.sign(user, config.jwt_secret, { noTimestamp: true })
        return res.status(200).json({
            user,
            token,
            tokenType: 'Bearer'
          })
      })
    }    
  })
})

module.exports = router;