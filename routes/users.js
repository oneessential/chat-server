const router = require('express').Router();
const User = require('../models/user');
const isEmail = require( "sane-email-validation" );

router.route("/users")
.get((req, res) => {
  User.find({}, { __v: 0, password: 0 }, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    return res.status(200).send(data)
  })
})
.post((req, res) => {
  let errorMsg = []
  if( !req.body.firstName ) {
    errorMsg.push("First name")
  }
  if( !req.body.lastName ) {
    errorMsg.push("Last name")
  }
  if( !req.body.email ) {
    errorMsg.push("Email")
  }
  if( !req.body.password ) {
    errorMsg.push("Password")
  }
  if( !req.body.confirmPassword ) {
    errorMsg.push("Confirm password")
  }
  if(errorMsg.length !== 0) {
    let message = errorMsg.join(', ')
    return res.status(400).json({message: "Provide " + message + " field"})
  }
  if(!isEmail(req.body.email)) {
    return res.status(401).json({message: "Email is not valid"})
  }
  if( req.body.password !== req.body.confirmPassword ) {
      return res.status(401).json({message: "Passwords didn't match"})
  }
  User.findOne({ "email": req.body.email }, (err, user) => {
    if(err) {
      return res.status(400).send(err);
    }
    if(user) {
      res.status(403).json({message: "User with this email already exist"})
    } else {
      let password = require('crypto')
                    .createHash('sha1')
                    .update(req.body.password)
                    .digest('base64');

      let newUser = User({
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        username : req.body.firstName + ' ' + req.body.lastName,
        email : req.body.email,
        password : password
      })

      newUser.save((err) => {
        if(err) {
          res.status(400).send(err);
        }
        res.status(201).json({message: "User created"})
      })
    }    
  })
})

router.route("/users/:id")
.get((req,res) => {
  User.findById(req.params.id, { __v: 0, password: 0 }, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    if(!data) {
      return res.status(404).json({message: "No users with ID: " + req.params.id});
    } else {
      return res.status(200).send(data);
    }
  })
})
.put((req,res) => {
  User.findById(req.params.id, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    if(!Object.getOwnPropertyNames(req.body).length) {
      return res.status(404).json({message: "No fields to update"})
    }
    if(!data) {
      return res.status(404).json({message: "No users with ID: " + req.params.id});
    } else {
      let firstNameToUpdate = data.firstName;
      let lastNameToUpdate = data.lastName;

      if( req.body.firstName !== undefined ) {
        data.firstName = req.body.firstName
        firstNameToUpdate = req.body.firstName
      }
      if( req.body.lastName !== undefined ) {
        data.lastName = req.body.lastName
        lastNameToUpdate = req.body.lastName
      }
      // if( req.body.email !== undefined && isEmail(req.body.email)) {
      //   data.email = req.body.email
      // }
      if(req.body.password !== undefined) {
        data.password = require('crypto')
                        .createHash('sha1')
                        .update(req.body.password)
                        .digest('base64');
      }
      if(req.body.firstName || req.body.lastName) {
        data.username = firstNameToUpdate + ' ' + lastNameToUpdate
      }
      data.save((err) => {
        if(err) {
          return res.status(400).send(err);
        } else {
          return res.status(200).json({message: "Data is updated for " + req.params.id});
        }
      })
    }
  })
})
.delete((req,res) => {
  User.findById(req.params.id, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    if(!data) {
      return res.status(404).json({message: "No user with ID: " + req.params.id + " to delete"});
    } else {
      User.remove({_id : req.params.id}, (err) => {
        if(err) {
          return res.status(400).send(err);
        } else {
          return res.status(200).json({message: "Data associated with " + req.params.id + " is deleted"});
        }
      })
    }
  })
})

module.exports = router;