const router = require('express').Router();
const Room = require('../models/room');
const User = require('../models/user');
const Message = require('../models/message');

router.route("/rooms")
.get((req, res) => {
  Room.find({}, { __v: 0 }, (err, data) => {
    if(err) {
      return res.status(400).send(err);
    }
    return res.status(200).json(data)
  })
})
.post((req, res) => {
  Room.findOne({ "members": { $all: [req.body.firstId, req.body.secondId] } }, (err, data) => {
    if(err) {
      return res.status(400).send(err)
    }
    if(data) {
      return res.status(403).json({message: "Room with this members already exist"})
    } else if(!req.body.firstId || !req.body.secondId) {
      return res.status(401).json({message: "Provide two valid ID's"})
    } else {
      let users = [req.body.firstId, req.body.secondId];

      User.find({"_id": {$in: users}}, { __v: 0, password: 0 }, (err, data) => {
        if(err) {
          return res.status(400).send(err)
        }
        let usersName = data.map(obj => obj.username);

        let newRoom = Room({
          members : [req.body.firstId, req.body.secondId],
          names: [...usersName]
        })

        newRoom.save((err, room) => {
          if(err) {
           return res.status(400).send(err)
          }
          return res.status(201).json({message: "Room " + room._id + " added successfully"})
        })
      })
    }
  })
})

router.route("/rooms/:id")
.get((req,res) => {
  Room.findById(req.params.id, { __v: 0 }, (err, data) => {
    if(err) {
      return res.status(400).send(err)
    }
    if(!data) {
      return res.status(404).json({message: "No room with ID: " + req.params.id})
    } else {
      return res.status(200).send(data);
    }
    
  })
})
.delete((req,res) => {
  Room.findById(req.params.id, (err, data) => {
    if(err) {
      return res.status(400).send(err)
    }
    if(!data) {
      return res.status(404).json({message: "No room with ID: " + req.params.id + " to delete"})
    } else {
      Room.remove({_id : req.params.id}, (err) => {
        if(err) {
          return res.status(400).send(err)
        } else {
          Message.remove({"roomId" : req.params.id}, (err) => {
            if(err) {
              return res.status(400).send(err)
            } else {
              return res.status(200).json("Data associated with " + req.params.id + " is deleted");
            }
          })
        }
      })
    }
  })
})

//get rooms containing user provided ID
router.route("/getrooms/:userId")
.get((req,res) => {
  Room.find({ "members": { $in: [req.params.userId] } }, { __v: 0 }, (err, data) => {
    if(err) {
      return res.status(400).send(err)
    }
    return res.status(200).send(data)
  })


})

router.route("/getonlinefriends/:userId")
.get((req,res) => {
  Room.find({ "members": { $in: [req.params.userId] } }, { __v: 0 }, (err, data) => {
    if(err || !data) {
      return res.status(400).send(err)
    }
    let members = [];
    data.map(room => members.push(room.members[0], room.members[1]))
    let friends = members.filter(member => member !== req.params.userId)
    
    User.find({"_id": {$in: friends}, "online": true }, (err, data) => {
      if(err) {
        return res.status(400).send(err)
      }
      if(!data) {
        return res.status(200).send(data)
      }
      let onlineUsers = data.map(user => user._id)
      return res.status(200).send(onlineUsers)
    })
  })
})

module.exports = router;