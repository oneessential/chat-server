const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let roomSchema = new Schema({
  members: [ String ],
  names: [ String ],
  created_at: { type: Number },
  updated_at: { type: Number }
})

roomSchema.pre('save', function(next) {

  var currentDate = Date.now();

  this.updated_at = currentDate;

  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

let Room = mongoose.model('Room', roomSchema);

module.exports = Room;
