const express = require('express');
const app = express();
const routes = require("./router");
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http');
const socket  = require('socket.io');
const server = http.createServer(app);
const io = socket.listen(server);
const jwt = require('jsonwebtoken');
const socketioJwt = require('socketio-jwt');
const config = require('./config.json');
const Message = require('./models/message');
const User = require('./models/user');
const port = process.env.PORT || 4000;

const mongoose = require("mongoose");
mongoose.connect(config.mongodb_url);
//local mongodb "mongodb_url" : "mongodb://127.0.0.1:27017/react-chat_db"

app.get('/', (req, res) => {
    res.redirect('/api')
})

app.use(cors());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use('/api', routes);

io.sockets
  .on('connection', socketioJwt.authorize({
    secret: config.jwt_secret,
		callback: false
  }))
  .on('authenticated', socket => {
    io.emit('join', {
      user: socket.decoded_token._doc,
      time: Date.now()
    })
		console.log("Joined User with ID: " + socket.decoded_token._doc._id)
		User.findByIdAndUpdate(socket.decoded_token._doc._id, { $set: { online: true }}, { new: true }, function (err) {
			if(err) {
	      io.emit('error', err)
	    }
		});
	
	socket
	  .on('message', chatMessageHandler)
	  .on('disconnect', disconnectHandler)
	  .on('delete-room', deleteRoomHandler)
	  .on('create-room', createRoomHandler)

	function chatMessageHandler(msg , roomId) {
	  const msgObj = Message({
	    msg: msg,
	    roomId: roomId,
	    user: socket.decoded_token._doc,
	    created_at: Date.now()
	  });

	  io.emit('message', msgObj)

	  msgObj.save((err) => {
	    if(err) {
	      io.emit('error', err)
	    }
	  })
	}

	function disconnectHandler () {
	  io.emit('leave', {
	    user: socket.decoded_token._doc,
	    time: Date.now()
	  })

		console.log("Leave User with ID: " + socket.decoded_token._doc._id)
		User.findByIdAndUpdate(socket.decoded_token._doc._id, { $set: { online: false }}, { new: true }, function (err) {
			if(err) {
	      io.emit('error', err)
	    }
		})
	}
	
	function createRoomHandler (roomData) {
		let secondUserId = roomData.secondId;
		io.emit('create-room', secondUserId)
	}	

	function deleteRoomHandler (roomId) {
		io.emit('delete-room', roomId)
	}
  })

server.listen(port, (err) => {
  if(err) {
    return console.log('Something went wrong!...')
  }
  console.log('Server is listening on http://localhost:' + port + "/api/")
})
